<p align="center">
  <a title="VASG site" href="https://IEEE-P1076.gitlab.io"><img src="https://img.shields.io/website.svg?label=IEEE-P1076.gitlab.io&longCache=true&style=flat-square&url=http%3A%2F%2FIEEE-P1076.gitlab.io%2Findex.html&logo=GitLab&logoColor=fff"></a><!--
  -->
  <a title="E-mail reflector/list" href="http://grouper.ieee.org/groups/1076/email"><img alt="E-mail reflector/list" src="https://img.shields.io/badge/-grouper.ieee.org/groups/1076-323131.svg?logo=ieee&style=flat-square&longCache=true"></a><!--
  -->
  <a title="EDA TWIKI P1076" href="http://www.eda-twiki.org/cgi-bin/view.cgi/P1076"><img alt="EDA TWIKI P1076" src="https://img.shields.io/badge/-eda--twiki.org-323131.svg?logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA%2FwD%2FAP%2BgvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QIWAhImtwMY5AAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAACwUlEQVRYw%2B1XTWgTQRT%2BZpOokURDg6GiaAwoCEX8QQpevFTQHgTBY8FrERSstKLePVSleLSH3qUFRSx48CR4CIJU8BC0WKqVULQiJsZmZ94%2BD83GJPOTDR68dGHZXYb3vu99882bWWDz%2Bs%2BXCF%2F408Au%2BD9GoaqXoIICFAQUAAWAGk%2FXTQCkAFSwhiAxD94%2BJS6vLEQiwOULp1B5Og9JGS2xbCEgDcDheOcYxQhb%2BibF2NdbTgL8%2BWQOv98sQlJaSyQ7qpeW6k0kQkWyR86Jq2%2Bf2wh4qK%2BMdgWXBnBpeaoWMpKB1dIztwKl9BL8Sl4DF6lXkMGyltT%2BruCrs%2FD9nJYre%2FCMuP3hhYlAHH5tvz5%2FAJL5B%2BL0u9leHM13My8h%2FZyWq1ot2KdAsdACFAAVxHpeUzZ%2F1Nc9BwHD3IXf%2F0og9A4Ja0jcCC4B8PfjPJuqNsfFtkUx8q3UlJvZw730eRBUI2YdvuzTTBrmdRIggwfqq%2BNQGG%2BuBtAkgBstsTHUfz1ur5j1FSEBJBBBAW2Nc8d0cKBFh4BdO6SLADk6XOs392A61YsCUcBVD6YzKeAwtOfs%2FW1O7kJAWjYtW6zmAVcFBCBwEJBdPKCimpAcVQkLARlxu3YSkBZg6pLAthWbVHWakAwHj04fmJopORSI2FXNCpBBjaBHBWQEBTf6gAdQ4PaATQFlUa6TmE9sJ4DkEmTlgBOcABD385XUMZAQUGBcP5To2v3CnIE3DOAhXxwUYq7I7QeS6d038bN8xwm%2B0QsYBO4Y89o2Hd%2BigAQwMDgl5opjeiOKZ6fBW9esFfxtRAISXgPUawO3kW7N97p4jQ%2FvfMQjw%2Fv0U%2FHM0X6U339BreZZk5gAQol9B7huygB7CvfFwseJ9v%2BCmaEMlosTUBhCfMcJqJY2TQ7DKccyNHVUBcAPgESSsDf%2FZPPXbPP6A13nhms8bYukAAAAAElFTkSuQmCC&style=flat-square&longCache=true"></a><!--
  -->
</p>

# IEEE P1076 WG: VHDL LRM Issues

The VHDL Analysis and Standardization Group (VASG) is responsible for maintaining and extending the VHDL standard (IEEE 1076).

- The **landing site** of the group is [IEEE-P1076.gitlab.io](https://IEEE-P1076.gitlab.io).
- Interested users can **subscribe to the reflector** (mailing-list) and/or **read the archives**: [grouper.ieee.org/groups/1076/email](http://grouper.ieee.org/groups/1076/email/thread.html).
- VHDL libraries are published at [opensource.ieee.org/vasg/Packages](https://opensource.ieee.org/vasg/Packages) (since December 2019).
  - Development for the next releases of the libraries is coordinated in [IEEE-P1076/packages](https://gitlab.com/IEEE-P1076/packages).
- Although being a member of IEEE is not required, writing to the reflector or accessing ieee.org require users to register. In order to reach a wider audience, the VASG uses an organization in GitLab: [gitlab.com/IEEE-P1076](https://gitlab.com/IEEE-P1076).

**This repository, VHDL-Issues, is for issues in the VHDL language, especially in the Language Reference Manual (LRM)**. Any user can request/propose/ask modifications to the upcoming revisions of the language.

> NOTE: even though not directly related to the VASG, an organization named “Open Source VHDL Group” exists in GitHub: [github.com/vhdl](https://github.com/vhdl) (find corresponding chat rooms: [gitter.im/vhdl](https://gitter.im/vhdl)).
