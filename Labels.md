# Labels and Issue Classification

## General

* ~"Discussion"
* ~"Duplicate"
* ~"LRM Reading"
* ~"Needs LRM Research"
* ~"Question"
* ~"Testcase"
* ~"Tool Problem"
* ~"TWIKI"
* For Meetings:
  * ~"Meeting::Agenda"
  * ~"Meeting::Minutes"
  * ~"Meeting::Action"
  * ~"Next Meeting"

## Workflow

* ~"Request for close"
* For regular 'Capability/Feature Request' issues:
  * ~"New" (set by template)
* For 'Bug Report' issues:
  * ~"Bug::Needs Confirmation" (set by template)
  * ~"Bug::Confirmed"
* For 'Formal Proposal', 'Bug Fix', or 'Language Change Specification' issues/MRs:
  * ~"Flow::New" (set by all three templates)
  * ~"Flow::Draft"
  * ~"Flow::Ready for Review"
  * ~"Flow::Approved"
  * ~"Flow::Rejected"
  * ~"Proposal" (set by template 'Proposal')
  * ~"Bug Fix" (set by template 'Bug Fix')
  * ~"LCS" (set by template 'Language Change Specification')
* For 'Bug Report' or 'Formal Proposal' which don't have a corresponding MR yet:
  * ~"Accepting Merge Requests"
* For 'Modification' groups:
  * ~"Change::*" (identifiers to be decided in meetings)

NOTE: if a ~"Bug::Confirmed" or ~"Proposal" issue is labeled with ~"Accepting Merge Requests", but the content to be
modified in the LRM is not converted to LaTeX yet, the procedure is as follows:

- Create an issue in [IEEE-P1076/lrm-latex](https://gitlab.com/IEEE-P1076/lrm-latex), explaining the content that is
missing in order to go forward with the implementation.
- Use issue linking features and make the new issue a blocking link of the Bug or Proposal.

## Proposal Intention

* ~"Enhancement"
* ~"Clarification"

## Revisions

* ~"VHDL: '87"
* ~"VHDL: '93"
* ~"VHDL: '08"
* ~"VHDL: '19"
* ~"Missed: VHDL'08"
* ~"Missed: VHDL'19"

## Technical

* ~"BNF"
* ~"PSL"
* ~"Typo"
* ~"Type Setting Issue"

## Quality

* ~"Language Quality: Ambiguity"
* ~"Language Quality: Inconsistency"
* ~"Language Quality: Coding Style"

## Features

* ~"Feature: Attribute"
  * ~"Attribute: Pre-defined"
  * ~"Attribute: User-defined"
* ~"Feature: Alias"
* ~"Feature: Design Unit"
  * ~"Design Unit: Entity"
  * ~"Design Unit: Architecture"
  * ~"Design Unit: Package"
  * ~"Design Unit: Package Body"
  * ~"Design Unit: Context"
  * ~"Design Unit: Configuration"
* ~"Feature: Component"
* ~"Feature: Conditional Analysis"
* ~"Feature: Encryption"
* ~"Feature: Environment"
* ~"Feature: External Name"
* ~"Feature: Mode View"
* ~"Feature: Overloading"
* ~"Feature: Range Record"
* ~"Feature: Reflection"
* ~"Feature: Type"
  * ~"Type: Access"
  * ~"Type: Array"
  * ~"Type: Conversion"
  * ~"Type: Incomplete"
  * ~"Type: Protected"
  * ~"Type: Record"
  * ~"Type: Scalar"
    * ~"Type: Scalar: Integer"
  * ~"Type: Derived (new)"
  * ~"Type: Union (new)"
  * ~"Type: Discriminants (new)"
* ~"Feature: Subprogram"
  * ~"Subprogram: Function"
  * ~"Subprogram: Method"
  * ~"Subprogram: Operator"
  * ~"Subprogram: Procedure"
* ~"Feature: API"
  * ~"API: DPI/FFI (new)"
  * ~"API: Python (new)"
  * ~"API: VHPI"

## Packages

* library `std`
  * ~"Package: env"
  * ~"Package: standard"
  * ~"Package: textio"
* library `ieee`
  * ~"Package: numeric_std"
  * ~"Package: std_logic_1164"

## Labels defined in a single repo

* LaTeX repo
  * lrm-latex~"IEEE Publishing"
  * lrm-latex~"Style"
  * lrm-latex~"Needs LaTeX Research"
  * lrm-latex~"LaTeX Bug"
* LRM repo
  * LRM~"D7 Reviewed"
  * LRM~"D7A Reviewed"
  * LRM~"D8 Reviewed"
  * LRM~"D10 Verified"
  * LRM~"Edited D6B: Please Review"
  * LRM~"Edited D6C: Please Review"
  * LRM~"Editing : Conflicting/unclear request"
  * LRM~"Edits Verified and Done"
  * LRM~"Logging Rejected Issues"
